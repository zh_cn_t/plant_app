package com.example.demo.exception;

public class PlantException extends RuntimeException {

    private String errMessage;

    public PlantException() {
    }

    public PlantException(String message) {
        super(message);
        this.errMessage = message;

    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public static void cast(String message) {
        throw new PlantException(message);
    }

    public static void cast(CommonError error) {
        throw new PlantException(error.getErrMessage());
    }
}

