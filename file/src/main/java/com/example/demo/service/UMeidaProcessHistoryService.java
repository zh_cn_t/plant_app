package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.po.UMeidaProcessHistory;

/**
* @author 25418
* @description 针对表【u_meida_process_history】的数据库操作Service
* @createDate 2023-06-07 18:00:20
*/
public interface UMeidaProcessHistoryService extends IService<UMeidaProcessHistory> {

}
