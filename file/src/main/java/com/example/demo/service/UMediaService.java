package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.po.*;

import java.io.File;

/**
* @author 25418
* @description 针对表【u_media】的数据库操作Service
* @createDate 2023-06-07 18:00:02
*/
public interface UMediaService extends IService<UMedia> {
    UploadFileResultDto upload(Integer contentId,String userId,UMedia uMedia,String localFilePath,String objectName);
    PageResult queryMediaFiels(String userId, PageParams pageParams, QueryMediaParamsDto queryMediaParamsDto);
    RestResponse<Boolean> checkFile(String fileMd5,int chunk);

    RestResponse<Boolean> checkChunk(String fileMd5, int chunk);

    RestResponse uploadChunk(String fileMd5, int chunk, String localFilePath);

    RestResponse mergechunks(Integer contentid, String id, String fileMd5, int chunkTotal, UMedia uMedia);
    boolean addUMediaToMinIO(String localFilePath,String mimeType,String bucket, String objectName);
    File downloadFileFromMinIO(String bucket, String objectName);
}
