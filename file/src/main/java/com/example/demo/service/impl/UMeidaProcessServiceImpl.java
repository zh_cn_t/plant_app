package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.UMediaMapper;
import com.example.demo.mapper.UMeidaProcessHistoryMapper;
import com.example.demo.mapper.UMeidaProcessMapper;
import com.example.demo.po.UMedia;
import com.example.demo.po.UMeidaProcess;
import com.example.demo.po.UMeidaProcessHistory;
import com.example.demo.service.UMeidaProcessService;
import groovy.util.logging.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
* @author 25418
* @description 针对表【u_meida_process】的数据库操作Service实现
* @createDate 2023-06-07 18:00:17
*/
@Service
@Slf4j
public class UMeidaProcessServiceImpl extends ServiceImpl
        <UMeidaProcessMapper, UMeidaProcess> implements UMeidaProcessService {

    @Autowired
    UMeidaProcessMapper uMeidaProcessMapper;
    @Autowired
    UMeidaProcessHistoryMapper uMeidaProcessHistoryMapper;
    @Autowired
    UMediaMapper uMediaMapper;
    @Override
    public List<UMeidaProcess> getMediaProcessList(int shardIndex, int shardTotal, int count){
        return uMeidaProcessMapper.selectListByShardIndex(shardTotal, shardIndex, count);
    }

    @Override
    public boolean startTask(long id) {
        int i = uMeidaProcessMapper.startTask(id);
        return i > 0;
    }

    @Override
    public void saveProcessFinishStatus(Long taskId, String status, String fileId, String url, String errorMsg) {
//根据taskId查出任务，如果不存在则直接返回
        UMeidaProcess uMediaProcess =uMeidaProcessMapper.selectById(taskId);
        if(uMediaProcess == null){
            return;
        }
        //使用LambdaQueryWrapper查询处理失败的id
        LambdaQueryWrapper<UMeidaProcess> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<UMeidaProcess> eq = lambdaQueryWrapper.eq(UMeidaProcess::getId,taskId);
        //判断处理失败的status是否为3 为3则更新任务处理失败 让失败次数+1
        if(status.equals("3")){
            UMeidaProcess uMediaProcess1 = new UMeidaProcess();
            uMediaProcess1.setStatus("3");
            uMediaProcess1.setErrormsg(errorMsg);
            uMediaProcess1.setFailCount(uMediaProcess.getFailCount()+1);
            uMeidaProcessMapper.update(uMediaProcess1,eq);
        }
        //处理成功
        //根据文件id查询url
        UMedia uMedia = uMediaMapper.selectById(fileId);
        if(uMedia!=null){
            //更新媒资文件中的访问url
            uMedia.setUrl(url);
            uMediaMapper.updateById(uMedia);
        }
        //处理成功，更新url和状态
        uMediaProcess.setUrl(url);
        uMediaProcess.setStatus("2");
        uMediaProcess.setFinishTime(LocalDateTime.now());
        //添加到历史记录
        UMeidaProcessHistory uMediaProcessHistory = new UMeidaProcessHistory();
        BeanUtils.copyProperties(uMediaProcess,uMediaProcessHistory);
        uMeidaProcessHistoryMapper.insert(uMediaProcessHistory);
        //删除uMediaProcess
        uMeidaProcessMapper.deleteById(uMediaProcess);
    }

}




