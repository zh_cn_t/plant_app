package com.example.demo.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.UUserInfoMapper;
import com.example.demo.po.UUserInfo;
import com.example.demo.service.UUserInfoService;
import org.springframework.stereotype.Service;

/**
* @author 25418
* @description 针对表【u_user_info】的数据库操作Service实现
* @createDate 2023-06-07 18:55:28
*/
@Service
public class UUserInfoServiceImpl extends ServiceImpl<UUserInfoMapper, UUserInfo> implements UUserInfoService {

}




