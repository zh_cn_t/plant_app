package com.example.demo.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.UMeidaProcessHistoryMapper;
import com.example.demo.po.UMeidaProcessHistory;
import com.example.demo.service.UMeidaProcessHistoryService;
import org.springframework.stereotype.Service;

/**
* @author 25418
* @description 针对表【u_meida_process_history】的数据库操作Service实现
* @createDate 2023-06-07 18:00:20
*/
@Service
public class UMeidaProcessHistoryServiceImpl extends ServiceImpl<UMeidaProcessHistoryMapper, UMeidaProcessHistory>
    implements UMeidaProcessHistoryService {

}




