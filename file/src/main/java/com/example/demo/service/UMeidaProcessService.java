package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.po.UMeidaProcess;

import java.util.List;

/**
* @author 25418
* @description 针对表【u_meida_process】的数据库操作Service
* @createDate 2023-06-07 18:00:17
*/
public interface UMeidaProcessService extends IService<UMeidaProcess> {
    /**
     * 获取待处理任务
     * @param shardIndex 获取待处理任务
     * @param shardTotal 分片总数
     * @param count 获取记录数
     * @return
     */
    List<UMeidaProcess> getMediaProcessList(int shardIndex, int shardTotal, int count);
    /**
     *  开启一个任务
     * @param id 任务id
     * @return true开启任务成功，false开启任务失败
     */
    boolean startTask(long id);
    /**
     * @description 保存任务结果
     * @param taskId  任务id
     * @param status 任务状态
     * @param fileId  文件id
     * @param url url
     * @param errorMsg 错误信息
     * @return void
     * @author Mr.M
     * @date 2022/10/15 11:29
     */
    void saveProcessFinishStatus(Long taskId,String status,String fileId,String url,String errorMsg);
}
