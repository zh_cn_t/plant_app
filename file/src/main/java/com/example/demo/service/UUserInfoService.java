package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.po.UUserInfo;

/**
* @author 25418
* @description 针对表【u_user_info】的数据库操作Service
* @createDate 2023-06-07 18:55:28
*/
public interface UUserInfoService extends IService<UUserInfo> {

}
