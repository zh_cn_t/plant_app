package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.po.UUserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 25418
* @description 针对表【u_user_info】的数据库操作Mapper
* @createDate 2023-06-07 18:55:28
* @Entity demo.po.UUserInfo
*/
@Mapper
public interface UUserInfoMapper extends BaseMapper<UUserInfo> {

}




