package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.po.UMeidaProcess;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
* @author 25418
* @description 针对表【u_meida_process】的数据库操作Mapper
* @createDate 2023-06-07 18:00:17
* @Entity demo.po.UMeidaProcess
*/
@Mapper
public interface UMeidaProcessMapper extends BaseMapper<UMeidaProcess> {
    /**
     * 根据分片参数获取待处理任务
     * @param shardTotal 分片总数
     * @param shardIndex 分片序号
     * @param count 任务数
     * @return
     */
    @Select("select * from u_media_process t " +
            "where t.id % #{shardTotal} " +
            "= #{shardIndex} " +
            "and (t.status = '1' " +
            "or t.status = '3') " +
            "and t.fail_count < 3 limit #{count}")
    List<UMeidaProcess> selectListByShardIndex
            (@Param("shardTotal") int shardTotal, @Param("shardIndex") int shardIndex,
             @Param("count") int count);

    /**
     * 开启一个任务
     * @param id 任务id
     * @return 更新记录数
     */
    @Update("update u_media_process m set m.status='4' where " +
            "(m.status='1' " + "or m.status='3') " +
            "and m.fail_count<3 and m.id=#{id}")
    int startTask(@Param("id") long id);
}




