package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.po.UMeidaProcessHistory;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 25418
* @description 针对表【u_meida_process_history】的数据库操作Mapper
* @createDate 2023-06-07 18:00:20
* @Entity demo.po.UMeidaProcessHistory
*/
@Mapper
public interface UMeidaProcessHistoryMapper extends BaseMapper<UMeidaProcessHistory> {

}




