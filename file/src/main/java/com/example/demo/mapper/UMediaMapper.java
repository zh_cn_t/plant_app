package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.po.UMedia;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 25418
* @description 针对表【u_media】的数据库操作Mapper
* @createDate 2023-06-07 18:00:02
* @Entity demo.po.UMedia
*/
@Mapper
public interface UMediaMapper extends BaseMapper<UMedia> {

}




