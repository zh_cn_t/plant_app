package com.example.demo.po;

import lombok.Data;
import lombok.ToString;

/**
 * @Classname QueryMediaParamsDto
 * @Description TODO
 * @Date 2023/6/7 20:19
 * @Created by Zh_Cn
 */
@Data
@ToString
public class QueryMediaParamsDto {

    private String filename;

    private String fileType;

    private Integer auditStatus;
}
