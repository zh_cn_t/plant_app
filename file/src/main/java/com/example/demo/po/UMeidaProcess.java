package com.example.demo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName u_meida_process
 */
@TableName(value ="u_meida_process")
@Data
public class UMeidaProcess implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 文件id
     */
    private String fileid;

    /**
     * 文件名称
     */
    private String filename;

    /**
     * 储存目录
     */
    private String bucket;

    /**
     * 储存路径
     */
    private String path;

    /**
     * 1:未处理2:处理成功3:处理失败
     */
    private String status;

    /**
     * 上传时间
     */
    private LocalDateTime createTime;

    /**
     * 完成时间
     */
    private LocalDateTime finishTime;

    /**
     * 失败次数
     */
    private Integer failCount;

    /**
     * 媒资文件访问地址
     */
    private String url;

    /**
     * 错误信息
     */
    private String errormsg;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}