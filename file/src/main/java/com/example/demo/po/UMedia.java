package com.example.demo.po;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName u_media
 */
@TableName(value ="u_media")
@Data
public class UMedia implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 内容id
     */
    private Integer contentid;

    /**
     * 文件名称
     */
    private String filename;

    /**
     * 文件类型
     */
    private String filetype;

    /**
     * 存储目录
     */
    private String bucket;

    /**
     * 存储路径
     */
    private String path;

    /**
     * 文件id
     */
    private String fileid;

    /**
     * 媒资文件访问地址
     */
    private String url;

    /**
     * 上传人
     */
    private String userid;

    /**
     * 上传时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 文件大小
     */
    private Long filesize;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 审核意见
     */
    private Integer status;
}