package com.example.demo.controller;

import com.example.demo.po.*;
import com.example.demo.service.UMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @Classname MediaFilesController
 * @Description TODO
 * @Date 2023/6/7 18:09
 * @Created by Zh_Cn
 */
@RestController
@CrossOrigin
public class MediaFilesController {
    @Autowired
    UMediaService uMediaService;

    /**
     * 图片上传
     * @param filedata
     * @param objectName
     * @param contentId
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/upload/coursefile",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public UploadFileResultDto upload
            (@RequestPart("filedata") MultipartFile filedata,
             @RequestParam(value= "objectName",required=false) String objectName,
             @RequestParam(value = "contentId",required = false)Integer contentId) throws IOException {
        UMedia uMedia = new UMedia();
        uMedia.setFilename(filedata.getOriginalFilename());
        uMedia.setFilesize(filedata.getSize());
        uMedia.setFiletype("图片");
        File tempFile = File.createTempFile("minio", ".temp");
        filedata.transferTo(tempFile);
//        SecurityUtil.UUserInfo user = SecurityUtil.getUser();
//        String id = String.valueOf(user.getId());
        String id = "1";
        String localFilePath = tempFile.getAbsolutePath();
        return uMediaService.upload(contentId,id,uMedia,localFilePath,objectName);
    }

    /**
     * 成功上传的媒资预览
     * @param pageParams
     * @param queryMediaParamsDto
     * @return
     */
    @PostMapping("/files")
    public PageResult<UMedia> list(PageParams pageParams, @RequestBody QueryMediaParamsDto queryMediaParamsDto){
//        SecurityUtil.UUserInfo user = SecurityUtil.getUser();
//        String id = String.valueOf(user.getId());
        String id = "1";
        return uMediaService.queryMediaFiels(id,pageParams,queryMediaParamsDto);
    }
}
