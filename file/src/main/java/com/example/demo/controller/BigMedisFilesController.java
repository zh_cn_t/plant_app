package com.example.demo.controller;


import com.example.demo.po.RestResponse;
import com.example.demo.po.UMedia;
import com.example.demo.service.UMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @Classname BigMedisFilesController
 * @Description TODO
 * @Date 2023/6/7 20:31
 * @Created by Zh_Cn
 */
@RestController
@CrossOrigin
public class BigMedisFilesController {

    @Autowired
    UMediaService uMediaService;

    /**
     * 上传前的检查
     * @param fileMd5
     * @return
     */
    @PostMapping("/upload/checkfile")
    public RestResponse<Boolean> checkfile( @RequestParam("fileMd5") String fileMd5,@RequestParam("chunk") int chunk
    ) {
        return uMediaService.checkFile(fileMd5,chunk);
    }

    /**
     * 分块文件上传前的检测
     * @param fileMd5
     * @param chunk
     * @return
     */
    @PostMapping("/upload/checkchunk")
    public RestResponse<Boolean> checkchunk(@RequestParam("fileMd5") String fileMd5,
                                            @RequestParam("chunk") int chunk) {
        RestResponse<Boolean> booleanRestResponse = uMediaService.checkChunk(fileMd5,chunk);
        return booleanRestResponse;
    }

    /**
     * 上传分块文件
     * @param file
     * @param fileMd5
     * @param chunk
     * @return
     * @throws Exception
     */
    @PostMapping("/upload/uploadchunk")
    public RestResponse uploadchunk(@RequestParam("file") MultipartFile file,
                                    @RequestParam("fileMd5") String fileMd5,
                                    @RequestParam("chunk") int chunk) throws Exception {

        //创建一个临时文件
        File tempFile = File.createTempFile("minio", ".temp");
        file.transferTo(tempFile);
        //文件路径
        String localFilePath = tempFile.getAbsolutePath();
        return uMediaService.uploadChunk(fileMd5, chunk, localFilePath);
    }

    /**
     * 合并文件
     * @param fileMd5
     * @param fileName
     * @param chunkTotal
     * @return
     */
    @PostMapping("/upload/mergechunks")
    public RestResponse mergechunks(@RequestParam("fileMd5") String fileMd5,
                                    @RequestParam("contentid") Integer contentid,
                                    @RequestParam("fileName") String fileName,
                                    @RequestParam("chunkTotal") int chunkTotal) {
//        SecurityUtil.UUserInfo user = SecurityUtil.getUser();
//        String id = String.valueOf(user.getId());
        String id = "1";
        //文件信息对象
        UMedia uMedia = new UMedia();
        uMedia.setFilename(fileName);
        uMedia.setFiletype("视频");
        return uMediaService.mergechunks(contentid,id, fileMd5, chunkTotal, uMedia);

    }
}
