package com.example.plant_app.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName u_comments
 */
@TableName(value ="u_comments")
@Data
public class UComments implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 评论内容
     */
    private String contentid;

    /**
     * 用户id
     */
    private Integer userid;

    /**
     * 星球id
     */
    private Integer plantid;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 评论状态1:正常 2:删除 
     */
    private Integer stauts;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}