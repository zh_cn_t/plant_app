package com.example.plant_app.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName u_planet
 */
@TableName(value ="u_planet")
@Data
public class UPlanet implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 星球名称
     */
    private String name;

    /**
     * 星球标签
     */
    private String tag;

    /**
     * 星球球主
     */
    private Integer userid;

    /**
     * 星球简介
     */
    private String introduction;

    /**
     * 星球头像
     */
    private String plantPic;

    /**
     * 星球价格
     */
    private Double price;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}