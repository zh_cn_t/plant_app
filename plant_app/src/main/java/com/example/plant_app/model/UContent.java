package com.example.plant_app.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 
 * @TableName u_content
 */
@TableName(value ="u_content")
@Data
public class UContent implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 星球id
     */
    private Integer plantid;

    /**
     * 用户id
     */
    private Integer userid;

    /**
     * 文字内容
     */
    private String text;

    /**
     * 图片内容
     */
    private String filePic;

    /**
     * 视频内容
     */
    private String fileVideo;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 用户状态1:正常 2:删除 
     */
    private Integer stauts;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}