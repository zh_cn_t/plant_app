package com.example.plant_app.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName u_members
 */
@TableName(value ="u_members")
@Data
public class UMembers implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 星球id
     */
    private Integer plantid;

    /**
     * 用户id
     */
    private Integer userid;

    /**
     * 加入星球时间
     */
    private LocalDateTime createTime;

    /**
     * 星球结束时间
     */
    private LocalDateTime endTime;

    /**
     * 用户状态1:正常 2:封禁 
     */
    private Integer stauts;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}