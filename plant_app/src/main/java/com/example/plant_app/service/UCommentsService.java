package com.example.plant_app.service;

import com.example.plant_app.model.UComments;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 25418
* @description 针对表【u_comments】的数据库操作Service
* @createDate 2023-06-08 12:02:58
*/
public interface UCommentsService extends IService<UComments> {

}
