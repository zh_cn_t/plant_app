package com.example.plant_app.service;

import com.example.plant_app.model.UUserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 25418
* @description 针对表【u_user_info】的数据库操作Service
* @createDate 2023-06-08 14:06:18
*/
public interface UUserInfoService extends IService<UUserInfo> {

}
