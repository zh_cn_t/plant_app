package com.example.plant_app.service;

import com.example.plant_app.model.UPlanet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 25418
* @description 针对表【u_planet】的数据库操作Service
* @createDate 2023-06-08 12:02:18
*/
public interface UPlanetService extends IService<UPlanet> {

    /**
     * 创建星球
     * @param uPlanet
     * @return
     */
    UPlanet createPlant(UPlanet uPlanet);
}
