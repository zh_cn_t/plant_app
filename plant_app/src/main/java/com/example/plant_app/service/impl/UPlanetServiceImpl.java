package com.example.plant_app.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.plant_app.exception.PlantException;
import com.example.plant_app.model.UPlanet;
import com.example.plant_app.service.UPlanetService;
import com.example.plant_app.mapper.UPlanetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
* @author 25418
* @description 针对表【u_planet】的数据库操作Service实现
* @createDate 2023-06-08 12:02:18
*/
@Service
public class UPlanetServiceImpl extends ServiceImpl<UPlanetMapper, UPlanet>
    implements UPlanetService{

    @Autowired
    UPlanetMapper uPlanetMapper;
    @Override
    public UPlanet createPlant(UPlanet uPlanet) {
        uPlanet.setCreateTime(LocalDateTime.now());
        int insert = uPlanetMapper.insert(uPlanet);
        if (insert<=0){
            PlantException.cast("新增失败");
        }
        return uPlanet;
    }
}




