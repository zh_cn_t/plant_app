package com.example.plant_app.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.plant_app.exception.PlantException;
import com.example.plant_app.model.UContent;
import com.example.plant_app.service.UContentService;
import com.example.plant_app.mapper.UContentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
* @author 25418
* @description 针对表【u_content】的数据库操作Service实现
* @createDate 2023-06-08 12:02:55
*/
@Service
public class UContentServiceImpl extends ServiceImpl<UContentMapper, UContent>
    implements UContentService{

    @Autowired
    UContentMapper uContentMapper;
    @Override
    public UContent release(UContent uContent) {
        uContent.setStauts(1);
        uContent.setCreateTime(LocalDateTime.now());
        uContent.setUpdateTime(LocalDateTime.now());
        int insert = uContentMapper.insert(uContent);
        if (insert<=0){
            PlantException.cast("发布帖子失败");
        }
        return uContent;
    }
}




