package com.example.plant_app.service;

import com.example.plant_app.model.UMembers;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.plant_app.model.UPlanet;

/**
* @author 25418
* @description 针对表【u_members】的数据库操作Service
* @createDate 2023-06-08 12:02:49
*/
public interface UMembersService extends IService<UMembers> {

    /**
     * 加入星球
     * @param plantId
     * @param userId
     * @return
     */
    UPlanet addUplant(Integer plantId, Integer userId);
}
