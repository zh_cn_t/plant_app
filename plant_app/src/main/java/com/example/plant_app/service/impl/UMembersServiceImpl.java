package com.example.plant_app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.plant_app.exception.PlantException;
import com.example.plant_app.mapper.UPlanetMapper;
import com.example.plant_app.model.UMembers;
import com.example.plant_app.model.UPlanet;
import com.example.plant_app.service.UMembersService;
import com.example.plant_app.mapper.UMembersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
* @author 25418
* @description 针对表【u_members】的数据库操作Service实现
* @createDate 2023-06-08 12:02:49
*/
@Service
public class UMembersServiceImpl extends ServiceImpl<UMembersMapper, UMembers>
    implements UMembersService{

    @Autowired
    UMembersMapper uMembersMapper;
    @Autowired
    UPlanetMapper uPlanetMapper;
    @Override
    public UPlanet addUplant(Integer plantId, Integer userId) {
        UPlanet uPlanet = uPlanetMapper.selectOne(new LambdaQueryWrapper<UPlanet>()
                .eq(UPlanet::getId, plantId));
        if (uPlanet==null){
            PlantException.cast("没有该星球");
        }
        UMembers uMembers = new UMembers();
        uMembers.setUserid(userId);
        uMembers.setStauts(1);
        uMembers.setPlantid(plantId);
        uMembers.setCreateTime(LocalDateTime.now());
        uMembers.setEndTime(LocalDateTime.now().plusDays(365));
        int insert = uMembersMapper.insert(uMembers);
        if (insert<=0){
            PlantException.cast("加入星球失败");
        }
        return uPlanet;
    }
}




