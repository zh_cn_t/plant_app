package com.example.plant_app.service;

import com.example.plant_app.model.UContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 25418
* @description 针对表【u_content】的数据库操作Service
* @createDate 2023-06-08 12:02:55
*/
public interface UContentService extends IService<UContent> {
    /**
     * 发布帖子
     * @param uContent
     * @return
     */
    UContent release(UContent uContent);
}
