package com.example.plant_app.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.plant_app.model.UComments;
import com.example.plant_app.service.UCommentsService;
import com.example.plant_app.mapper.UCommentsMapper;
import org.springframework.stereotype.Service;

/**
* @author 25418
* @description 针对表【u_comments】的数据库操作Service实现
* @createDate 2023-06-08 12:02:58
*/
@Service
public class UCommentsServiceImpl extends ServiceImpl<UCommentsMapper, UComments>
    implements UCommentsService{

}




