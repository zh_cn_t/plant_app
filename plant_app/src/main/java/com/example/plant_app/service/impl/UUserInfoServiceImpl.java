package com.example.plant_app.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.plant_app.model.UUserInfo;
import com.example.plant_app.service.UUserInfoService;
import com.example.plant_app.mapper.UUserInfoMapper;
import org.springframework.stereotype.Service;

/**
* @author 25418
* @description 针对表【u_user_info】的数据库操作Service实现
* @createDate 2023-06-08 14:06:18
*/
@Service
public class UUserInfoServiceImpl extends ServiceImpl<UUserInfoMapper, UUserInfo>
    implements UUserInfoService{

}




