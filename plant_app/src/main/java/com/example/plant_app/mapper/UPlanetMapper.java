package com.example.plant_app.mapper;

import com.example.plant_app.model.UPlanet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 25418
* @description 针对表【u_planet】的数据库操作Mapper
* @createDate 2023-06-08 12:02:18
* @Entity com.example.plant_app.model.UPlanet
*/
@Mapper
public interface UPlanetMapper extends BaseMapper<UPlanet> {

}




