package com.example.plant_app.controller;

import com.example.plant_app.model.UPlanet;
import com.example.plant_app.service.UMembersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname MembersController
 * @Description TODO
 * @Date 2023/6/8 12:34
 * @Created by Zh_Cn
 */
@RestController
public class MembersController {

    @Autowired
    UMembersService uMembersService;
    @PostMapping("/add")
    public UPlanet addUplant(@RequestBody Integer plantId){
//        SecurityUtil.UUserInfo user = SecurityUtil.getUser();
//        String userId = String.valueOf(user.getId());
        Integer userId = 1;
        return uMembersService.addUplant(plantId,userId);
    }
}
