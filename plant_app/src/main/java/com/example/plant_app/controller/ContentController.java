package com.example.plant_app.controller;

import com.example.plant_app.model.UContent;
import com.example.plant_app.service.UContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname ContentController
 * @Description TODO
 * @Date 2023/6/8 12:33
 * @Created by Zh_Cn
 */
@RestController
public class ContentController {

    @Autowired
    UContentService uContentService;
    @PostMapping("/release")
    public UContent release(@RequestBody UContent uContent){
//        SecurityUtil.UUserInfo user = SecurityUtil.getUser();
//        String userId = String.valueOf(user.getId());
        Integer userId = 1;
        uContent.setUserid(userId);
        return uContentService.release(uContent);
    }
}
