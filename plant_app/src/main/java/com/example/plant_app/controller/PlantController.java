package com.example.plant_app.controller;
import com.example.plant_app.model.UPlanet;
import com.example.plant_app.service.UPlanetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname PlantController
 * @Description TODO
 * @Date 2023/6/8 12:34
 * @Created by Zh_Cn
 */
@RestController
public class PlantController {
    @Autowired
    UPlanetService uPlanetService;

    @PostMapping("/create")
    public UPlanet createPlant(@RequestBody UPlanet uPlanet){
//        SecurityUtil.UUserInfo user = SecurityUtil.getUser();
//        String userId = String.valueOf(user.getId());
        Integer userId = 1;
        uPlanet.setUserid(userId);
        return uPlanetService.createPlant(uPlanet);
    }

}
